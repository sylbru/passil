const gulp = require("gulp")
const elm = require("gulp-elm")
const uglify = require("gulp-uglify")

function watch(cb) {
    gulp.watch(["src/*.elm"], function (cb) {
        console.log("Recompiling…")
        build(() => { console.log("…Done.") })
    })
    cb()
}

function build(cb) {
    gulp.src(["src/*.elm"])
            .pipe(elm())
            .pipe(uglify({ compress: false, mangle: false }))
            .pipe(gulp.dest("dist/js"))
    cb()
}

exports.default = watch
exports.watch = watch
exports.build = build