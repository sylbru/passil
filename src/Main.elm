port module Main exposing (..)

import Browser
import Element exposing (Element)
import Element.Background
import Element.Border
import Element.Font
import Element.Input exposing (OptionState(..))
import Element.Region
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Random
import Random.Extra
import Random.Set
import Set exposing (Set)
import String


type alias Model =
    { password : String
    , options : Options
    , optionsPanelVisible : Bool
    , justCopied : Bool
    }


type alias Options =
    { syllablesPerWord : Int
    , wordCount : Int
    , delimiter : String
    , prefix : String
    , syllableType : SyllableType
    , capitalization : Capitalization
    }


type SyllableType
    = VowelFirst
    | ConsonantFirst


type Capitalization
    = None
    | FirstWord
    | AllWords


type Msg
    = Generate
    | RandomResult String
    | UpdateSyllablesPerWord String
    | UpdateWordCount String
    | UpdateDelimiter String
    | UpdatePrefix String
    | UpdateSyllableType SyllableType
    | UpdateCapitalization Capitalization
    | ToggleOptionPanel
    | Copy


commonConsonants : List Char
commonConsonants =
    -- removed 'c', 'q' for less confusion
    [ 'b', 'd', 'f', 'g', 'h', 'j', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v' ]


rareConsonants : List Char
rareConsonants =
    [ 'k', 'w', 'x', 'z' ]


vowels : Set Char
vowels =
    [ 'a', 'e', 'i', 'o', 'u' ]
        -- removed 'y' for less confusion
        |> Set.fromList


initModel : Model
initModel =
    { password = ""
    , options =
        { syllablesPerWord = 3
        , wordCount = 3
        , delimiter = "-"
        , prefix = ""
        , syllableType = ConsonantFirst
        , capitalization = None
        }
    , optionsPanelVisible = False
    , justCopied = False
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( initModel, Random.generate RandomResult (randomPassword initModel.options) )


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view =
            view
                >> Element.layoutWith { options = [ focusStyle ] }
                    [ Element.Background.color <| Element.rgb255 0xEE 0xEE 0xEE
                    , Element.Font.family
                        [ Element.Font.typeface "Menlo"
                        , Element.Font.typeface "Consolas"
                        , Element.Font.typeface "Monaco"
                        , Element.Font.typeface "Liberation Mono"
                        , Element.Font.typeface "Lucida Console"
                        , Element.Font.monospace
                        ]
                    ]
        , subscriptions = subscriptions
        }


focusStyle : Element.Option
focusStyle =
    Element.focusStyle
        { backgroundColor = Nothing
        , borderColor = Nothing
        , shadow =
            Just
                { color = Element.rgb255 180 230 180
                , offset = ( 0, 0 )
                , blur = 0
                , size = 3
                }
        }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Generate ->
            generatePassword model

        RandomResult result ->
            ( { model | password = result }
            , Cmd.none
            )

        UpdateSyllablesPerWord value ->
            ( { model | options = setOptionSyllablesPerWord value model.options }
            , Cmd.none
            )

        UpdateWordCount value ->
            ( { model | options = setOptionWordCount value model.options }
            , Cmd.none
            )

        UpdateDelimiter value ->
            ( { model | options = setOptionDelimiter value model.options }
            , Cmd.none
            )

        UpdatePrefix value ->
            ( { model | options = setOptionPrefix value model.options }
            , Cmd.none
            )

        UpdateSyllableType syllableType ->
            ( { model | options = setOptionSyllableType syllableType model.options }
            , Cmd.none
            )

        UpdateCapitalization capitalization ->
            ( { model | options = setOptionCapitalization capitalization model.options }, Cmd.none )

        ToggleOptionPanel ->
            { model | optionsPanelVisible = not model.optionsPanelVisible }
                |> regenerateIfBackToHome

        Copy ->
            ( { model | justCopied = True }, copy model.password )


regenerateIfBackToHome : Model -> ( Model, Cmd Msg )
regenerateIfBackToHome model =
    if not model.optionsPanelVisible then
        -- We are back in the home page, we want to regenerate a password now
        generatePassword model

    else
        ( model, Cmd.none )


generatePassword : Model -> ( Model, Cmd Msg )
generatePassword model =
    ( model |> resetJustCopied
    , Random.generate RandomResult (randomPassword model.options)
    )


setOptionSyllablesPerWord : String -> Options -> Options
setOptionSyllablesPerWord value options =
    { options | syllablesPerWord = String.toInt value |> Maybe.withDefault 1 }


setOptionWordCount : String -> Options -> Options
setOptionWordCount value options =
    { options | wordCount = String.toInt value |> Maybe.withDefault 1 }


setOptionDelimiter : String -> Options -> Options
setOptionDelimiter value options =
    { options | delimiter = value }


setOptionPrefix : String -> Options -> Options
setOptionPrefix value options =
    { options | prefix = String.filter Char.isAlpha value |> String.toLower }


setOptionSyllableType : SyllableType -> Options -> Options
setOptionSyllableType syllableType options =
    { options | syllableType = syllableType }


setOptionCapitalization : Capitalization -> Options -> Options
setOptionCapitalization capitalization options =
    { options | capitalization = capitalization }


resetJustCopied : Model -> Model
resetJustCopied model =
    { model | justCopied = False }


randomPassword : Options -> Random.Generator String
randomPassword options =
    case String.toList <| String.reverse options.prefix of
        [] ->
            randomWord options
                |> List.repeat options.wordCount
                |> Random.Extra.combine
                |> Random.map (String.join options.delimiter)
                |> Random.map (capitalizePassword options.capitalization)

        lastChar :: _ ->
            let
                addedSyllablesToPrefix =
                    (5 - (String.length options.prefix // 2))
                        |> Basics.max 0
            in
            (if isVowel lastChar then
                randomConsonantFirstSyllable

             else
                randomVowelFirstSyllable
            )
                |> List.repeat addedSyllablesToPrefix
                |> Random.Extra.combine
                |> Random.map String.concat
                |> Random.map ((++) options.prefix)
                |> (\firstWordGenerator -> firstWordGenerator :: List.repeat (options.wordCount - 1) (randomWord options))
                |> Random.Extra.combine
                |> Random.map (String.join options.delimiter)
                |> Random.map (capitalizePassword options.capitalization)


randomVowel : Random.Generator Char
randomVowel =
    Random.Set.sample vowels |> Random.map (Maybe.withDefault ' ')


randomConsonant : Random.Generator Char
randomConsonant =
    let
        weightedConsonants =
            [ List.map (\c -> ( 3, c )) commonConsonants
            , List.map (\c -> ( 1, c )) rareConsonants
            ]
                |> List.concat
    in
    case weightedConsonants of
        first :: rest ->
            Random.weighted first rest

        [] ->
            Random.constant '⌧'


randomVowelFirstSyllable : Random.Generator String
randomVowelFirstSyllable =
    [ randomVowel, randomConsonant ]
        |> Random.Extra.combine
        |> Random.map String.fromList


randomConsonantFirstSyllable : Random.Generator String
randomConsonantFirstSyllable =
    [ randomConsonant, randomVowel ]
        |> Random.Extra.combine
        |> Random.map String.fromList


randomWord : Options -> Random.Generator String
randomWord options =
    let
        randomSyllable =
            case options.syllableType of
                VowelFirst ->
                    randomVowelFirstSyllable

                ConsonantFirst ->
                    randomConsonantFirstSyllable
    in
    List.repeat options.syllablesPerWord randomSyllable
        |> Random.Extra.combine
        |> Random.map String.concat
        |> Random.map (capitalizeWord options.capitalization)


capitalizeWord : Capitalization -> String -> String
capitalizeWord capitalization word =
    if capitalization == AllWords then
        capitalize word

    else
        word


capitalizePassword : Capitalization -> String -> String
capitalizePassword capitalization password =
    if capitalization == FirstWord then
        capitalize password

    else
        password


capitalize : String -> String
capitalize word =
    let
        firstLetter =
            String.left 1 word

        rest =
            String.dropLeft 1 word
    in
    String.append (String.toUpper firstLetter) rest


view : Model -> Element Msg
view model =
    if model.optionsPanelVisible then
        viewOptions model

    else
        viewMain model


viewMain : Model -> Element Msg
viewMain model =
    Element.column
        [ Element.Region.mainContent
        , Element.spaceEvenly
        , Element.width Element.fill
        , Element.height Element.fill
        , Element.padding globalPadding
        ]
    <|
        [ Element.el
            [ Element.Region.heading 1
            , Element.Font.letterSpacing 8
            , Element.centerX
            , Element.padding 16
            , Element.Font.color <| Element.rgb255 85 85 85
            ]
            (Element.text (String.toUpper "Passil"))
        , Element.paragraph
            [ Element.Font.family [ Element.Font.monospace ]
            , Element.Font.size 36
            , Element.Background.color (Element.rgb255 68 68 68)
            , Element.Font.color (Element.rgb 1 1 1)
            , Element.padding 16
            , Element.Border.rounded 5
            , Element.centerX
            , Element.Font.center
            , Element.width (Element.fill |> Element.maximum (27 * String.length model.password))
            ]
            [ Element.text model.password ]
        , Element.row
            [ Element.width Element.fill, Element.spacing 25 ]
            [ Element.Input.button
                [ Element.Background.color green
                , Element.Font.color (Element.rgb 1 1 1)
                , Element.paddingXY 13 10
                , Element.Border.rounded 3
                , Element.centerX
                , Element.mouseOver [ Element.Background.color (Element.rgb255 0x58 0xA7 0x58) ]
                ]
                { onPress = Just Generate
                , label = Element.text "Generate" |> fixButtonTextAlignment
                }
            , Element.Input.button
                [ Element.Border.color green
                , Element.Border.width 2
                , Element.Font.color green
                , Element.paddingXY 13 10
                , Element.Border.rounded 3
                , Element.centerX
                , if model.justCopied then
                    Element.alpha 0.5

                  else
                    Element.alpha 1
                , Element.mouseOver [ Element.Font.color (Element.rgb255 0 0 0), Element.Border.color (Element.rgb255 0 0 0) ]
                ]
                { onPress =
                    if model.justCopied then
                        Nothing

                    else
                        Just Copy
                , label = Element.text "Copy" |> fixButtonTextAlignment
                }
            ]
        , Element.Input.button
            [ Element.centerX
            , Element.Font.size 30
            , Element.padding 15
            ]
            { onPress = Just ToggleOptionPanel
            , label = Element.el [ Element.Region.description "Options" ] (Element.text "⚙") |> fixButtonTextAlignment
            }
        ]


green : Element.Color
green =
    Element.rgb255 72 135 72


fixButtonTextAlignment : Element msg -> Element msg
fixButtonTextAlignment element =
    Element.el
        [ Element.htmlAttribute <| Html.Attributes.style "transform" "translateY(.05em)" ]
        element


viewOptions : Model -> Element Msg
viewOptions model =
    Element.column
        [ Element.width (Element.fill |> Element.maximum 700)
        , Element.height Element.fill
        , Element.spaceEvenly
        , Element.centerX
        , Element.paddingEach { top = 50, right = 50, bottom = globalPadding, left = 50 }
        ]
    <|
        [ Element.Input.text
            [ Element.htmlAttribute (Html.Attributes.type_ "number")
            , Element.htmlAttribute (Html.Attributes.min "1")
            , Element.htmlAttribute (Html.Attributes.max "5")
            , Element.width Element.fill
            ]
            { onChange = UpdateSyllablesPerWord
            , text = String.fromInt model.options.syllablesPerWord
            , placeholder = Nothing
            , label = Element.Input.labelLeft labelAttributes (Element.text "Syllables per word")
            }
        , Element.Input.text
            [ Element.htmlAttribute (Html.Attributes.type_ "number")
            , Element.htmlAttribute (Html.Attributes.min "1")
            , Element.htmlAttribute (Html.Attributes.max "5")
            , Element.width Element.fill
            ]
            { onChange = UpdateWordCount
            , text = String.fromInt model.options.wordCount
            , placeholder = Nothing
            , label = Element.Input.labelLeft labelAttributes (Element.text "Word count")
            }
        , Element.Input.text
            [ Element.width Element.fill ]
            { onChange = UpdateDelimiter
            , text = model.options.delimiter
            , placeholder = Nothing
            , label = Element.Input.labelLeft labelAttributes (Element.text "Delimiter")
            }
        , Element.Input.text
            [ Element.width Element.fill ]
            { onChange = UpdatePrefix
            , text = model.options.prefix
            , placeholder = Nothing
            , label = Element.Input.labelLeft labelAttributes (Element.text "Prefix")
            }
        , Element.Input.radio
            [ Element.width Element.fill, Element.spacing 15, Element.htmlAttribute <| Html.Attributes.style "justify-content" "space-between" ]
            { onChange = UpdateSyllableType
            , selected = Just model.options.syllableType
            , label = Element.Input.labelLeft labelAttributes (Element.text "Syllable type")
            , options =
                [ Element.Input.optionWith VowelFirst (radioOption (Element.text "Vowel first"))
                , Element.Input.optionWith ConsonantFirst (radioOption (Element.text "Consonant first"))
                ]
            }
        , Element.Input.radio
            [ Element.width Element.fill, Element.spacing 15, Element.htmlAttribute <| Html.Attributes.style "justify-content" "space-between" ]
            { onChange = UpdateCapitalization
            , selected = Just model.options.capitalization
            , label = Element.Input.labelLeft labelAttributes (Element.text "Capitalization")
            , options =
                [ Element.Input.optionWith None (radioOption (Element.text "None"))
                , Element.Input.optionWith FirstWord (radioOption (Element.text "First word"))
                , Element.Input.optionWith AllWords (radioOption (Element.text "All words"))
                ]
            }
        , Element.Input.button
            [ Element.centerX
            , Element.Font.size 30
            , Element.padding 15
            ]
            { onPress = Just ToggleOptionPanel
            , label = Element.el [ Element.Region.description "Generator" ] (Element.text "✕") |> fixButtonTextAlignment
            }
        ]


radioOption : Element msg -> OptionState -> Element msg
radioOption label status =
    Element.row
        [ Element.spacing 15
        , Element.alignLeft
        , Element.width Element.shrink
        ]
        [ Element.el
            [ Element.width (Element.px 14)
            , Element.height (Element.px 14)
            , Element.Background.color (Element.rgb 1 1 1)
            , Element.Border.rounded 7
            , Element.Border.width <|
                case status of
                    Idle ->
                        1

                    Focused ->
                        1

                    Selected ->
                        5
            , Element.Border.color <|
                case status of
                    Idle ->
                        Element.rgb (208 / 255) (208 / 255) (208 / 255)

                    Focused ->
                        Element.rgb (208 / 255) (208 / 255) (208 / 255)

                    Selected ->
                        green
            ]
            Element.none
        , label
        ]


globalPadding : Int
globalPadding =
    20


labelAttributes : List (Element.Attribute Msg)
labelAttributes =
    [ Element.width Element.fill
    , Element.Font.bold
    ]


isVowel : Char -> Bool
isVowel char =
    List.member char [ 'a', 'e', 'i', 'o', 'u', 'y' ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


port copy : String -> Cmd msg
